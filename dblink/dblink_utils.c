
#include "../includes/push_swap.h"

t_stack_node *ft_lst_lastnode(t_stack_node *currlist) 
{
  while (currlist) 
	{
    if (!currlist->next)
      return (currlist);
    currlist = currlist->next;
  }
  return (currlist);
}

t_stack_node *ft_lst_firstnode(t_stack_node *currlist) 
{
  while (currlist) 
	{
    if (!currlist->prev)
      return (currlist);
    currlist = currlist->prev;
  }
  return (currlist);
}

t_stack_node *ft_lst_prevnode(t_stack_node *currlist) 
{

  currlist = currlist->prev;
  return (currlist);
}

t_stack_node *ft_lst_nextnode(t_stack_node *currlist) 
{
  currlist = currlist->next;
  return (currlist);
}

int ft_lst_lenght(t_stack_node *currlist) 
{
  int len;
  len = 0;
  while (currlist) {
    currlist = currlist->next;
    len++;
  }

  return (len);
}
