#include "stdio.h"
#include "stdlib.h"
#include "../includes/push_swap.h"

t_stack_node *node_init(void *content) 
{
  t_stack_node *link;

  link = (t_stack_node *)malloc(sizeof(*link));
  if (!link)
    return (NULL);
  link->data = content;
  link->prev = NULL;
  link->next = NULL;
  return (link);
}

void ft_lst_add_frontd(t_stack_node **currlist, t_stack_node *newnode) {
  if (currlist) {
    if (*currlist) {
      newnode->next = *currlist;
    }
    *currlist = newnode;
  }
}

void ft_lstiterd(t_stack_node *currlist, void (*f)(int *)) {
  if (!f)
    return;
  while (currlist) {
    (*f)(&currlist->data);
    currlist = currlist->next;
  }
}

void ft_lst_add_backd(t_stack_node **currlist, t_stack_node *node) {
  t_stack_node *last;

  if (currlist) {
    if (*currlist) {
      last = ft_lst_lastnode(*currlist);
      last->next = node;
      node->prev = last;
    } else
      *currlist = node;
  }
}

void ft_clearnode(t_stack_node *currlist, void (*del)(int *)) {
  if (currlist) {
    (*del)(&currlist->data);
    free(currlist);
  }
}

void ft_cleart_stack_node(t_stack_node **currlist, void (*del)(int *)) {
  t_stack_node *iter;

  if (!del || !currlist || !*currlist) {
    return;
  }
  // am not sure if it clear all of the list;
  while (currlist && *currlist) {
    iter = (*currlist)->next;
    *currlist = iter;
    ft_clearnode(*currlist, del);
  }
}

/*
int main(void)
{
t_stack_node *lst;
lst = NULL;

 ft_lst_add_backd(&lst,node_init("stuff"));
 ft_lst_add_backd(&lst,node_init("hello"));
 ft_lst_add_backd(&lst,node_init("please"));
 ft_lst_add_backd(&lst,node_init("help me"));
lst = ft_lst_lastnode(lst);
printf("%s",(char *)lst->content);
}
*/
