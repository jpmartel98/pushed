NAME = push_swap

FLAGS = -Wall -Wextra -Werror
SRCS = ./srcs/create_n_print_array.c \
	   ./srcs/create_stack.c ./srcs/double_linked_list_function.c  \
	   ./srcs/error_message.c ./srcs/free_arg.c ./srcs/move.c ./srcs/outils.c \
	   ./srcs/push_swap.c ./srcs/stack_size_calculator.c ./srcs/ft_split.c  ./dblink/dblink.c ./dblink/dblink_utils.c
	   
OBJS = ${SRCS:.c=.o}
CC = gcc 
all: ${NAME}

${NAME}:${OBJS} 
		@${CC} ${OBJS} ${FLAGS} -o ${NAME}

clean:
	    @${RM} ${OBJS}	

fclean: clean
	@${RM} ${NAME}
re: fclean all
	
.PHONY: clean fclean re all
